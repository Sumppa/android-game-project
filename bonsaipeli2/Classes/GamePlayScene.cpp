/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "GamePlayScene.h"
#include "SimpleAudioEngine.h"
#include "SettingsScene.h"
#include "GameStartScene.h"
#include "HelloWorldScene.h"
#include "AppDelegate.h"
#include "Globals.h"
#include "ShopScene.h"
#include <math.h>
#include <thread>

USING_NS_CC;

Scene* GamePlay::createScene()
{
    auto scene = Scene::createWithPhysics();
    scene->getPhysicsWorld()->setGravity(Vec2(0,-1000));
    auto layer = GamePlay::create();
    layer->SetPhysicsWorld(scene->getPhysicsWorld());

    scene->addChild(layer);

    return scene;

    // return GamePlay::createWithPhysics();

}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in GamePlayScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GamePlay::init()
{
    // super init
    if ( !Scene::init() )
    {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);

    //get current treetype
    treetype = def->getIntegerForKey("TREETYPE", 1);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //timers for sprinkler money and water bill
    float time = 0.6f;
    float sprinkTime = 0.5f;

    //chosen bg
    if (treetype == 1) {
        bg = Sprite::create("bg_moon.png");
        sprinkTime = 0.5f; }
    else if (treetype == 2) {
        bg = Sprite::create("bg_sun.png");
        sprinkTime = 0.3f; }
    else if (treetype == 3) {
        bg = Sprite::create("bg2.png");
        sprinkTime = 0.4f; }
    this->schedule(schedule_selector(GamePlay::updateWater), time);
    this->schedule(schedule_selector(GamePlay::sprinklerEffect), sprinkTime);
    bg->setPosition(Vec2(origin.x + visibleSize.width/2,
                         origin.y + visibleSize.height/2));
    this->addChild(bg);

    //get current exp, maxexp, leverl and money
    experience = def->getFloatForKey("EXP", 0);
    maxExperience = def->getFloatForKey("MAXEXP", 10);
    level = def->getIntegerForKey("LEVEL", 1);
    money = def->getIntegerForKey("MONEY", 0);
    percent = def->getFloatForKey("PERCENT", 0);

    //load item effects
    effect0 = def->getBoolForKey("HAT_STATE", false);
    effect1 = def->getIntegerForKey("MTWATER_FX", 0);
    effect2 = def->getIntegerForKey("HOLYWATER_FX", 0);
    effect3 = def->getIntegerForKey("UNHOLYWATER_FX", 0);
    effect4 = def->getIntegerForKey("WINE_FX", 0);
    effect5 = def->getIntegerForKey("GODWINE_FX", 0);

    //load number of sprinks
    sprinkcount = def->getIntegerForKey("SPRINK_AM", 0);

    //money and level texts
    moneycount = cocos2d::Label::createWithTTF("#", "fonts/Ubuntu-B.ttf", 14);
    moneycount->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    moneycount->setString("$: " + std::to_string(money));
    moneycount->setPosition(Vec2(origin.x + visibleSize.width/2,
                                 origin.y + visibleSize.height - moneycount->getContentSize().height));
    addChild(moneycount);

    levelcount = cocos2d::Label::createWithTTF("LVL: " + std::to_string(level), "fonts/Ubuntu-B.ttf", 14);
    levelcount->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    levelcount->setPosition(Vec2(origin.x + visibleSize.width/8,
                                 origin.y + visibleSize.height - moneycount->getContentSize().height));
    addChild(levelcount);

    //seed and tree
    if (def->getBoolForKey("GAMESTATE", false) == false) {
        seed = Sprite::create("seed.png");
        seed->setAnchorPoint(Vec2(0,0));
        seed->setPosition(Point(230,320));
        this->addChild(seed);
        auto moveseed = MoveBy::create(1, Point(0, -500));
        seed->runAction(moveseed);

        tree = Sprite::create("tree" + std::to_string(level) + ".png");
        tree->setAnchorPoint(Vec2(0.5f,0));
        tree->setPosition(Point(origin.x + visibleSize.width/2,-330));
        tree->setTag(10);
        this->addChild(tree);
        auto movetree = MoveBy::create(2, Point(0, 370));
        tree->runAction(movetree);
    }
    else {
        tree = Sprite::create("tree" + std::to_string(level) + ".png");
        tree->setAnchorPoint(Vec2(0.5f,0));
        tree->setPosition(Point(origin.x + visibleSize.width/2,40));
        tree->setTag(10);
        this->addChild(tree);

    }

    //check if game is started
    def->setBoolForKey("GAMESTATE", true);
    def->flush();
    if (effect0) {
        hat = Sprite::create("hat.png");
        tree->addChild(hat);
        hat->setAnchorPoint(Vec2(0.5f,0.5f));
        hat->setPosition(Point(tree->getContentSize().width-15,tree->getContentSize().height-4));
    }

    //exp bar
    loadingbar = ui::LoadingBar::create("progbar.png");
    loadingbar->setDirection(ui::LoadingBar::Direction::LEFT);
    loadingbar->setAnchorPoint(Point(0,0));
    loadingbar->setPosition(Point(origin.x, 43));
    loadingbar->setPercent(percent);
    barWidth = loadingbar->getContentSize().width;
    scaleFactor = Director::getInstance()->getVisibleSize().width / barWidth;
    loadingbar->setScaleX(scaleFactor);
    this->addChild(loadingbar);
    percent = loadingbar->getPercent();
    loadingbar->setPercent(def->getFloatForKey("PERCENT", 0));

    //toolbar
    auto toolbarrect = DrawNode::create();
    float lineWidth = 5.3 * CC_CONTENT_SCALE_FACTOR();
    toolbarrect->setAnchorPoint(Point(0,0));
    toolbarrect->drawSegment(Vec2(0, 20), Vec2(500, 20), lineWidth, Color4F::GRAY);
    addChild(toolbarrect);

    //variables float to int
    xp = experience;
    maxXp = maxExperience;

    //exp and maxexp labels
    expcount = cocos2d::Label::createWithTTF(std::to_string(xp) + "/" + std::to_string(maxXp), "fonts/Ubuntu-B.ttf", 8);
    expcount->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    expcount->setPosition(Vec2(origin.x + visibleSize.width / 2,
                               origin.y + visibleSize.height / 8.5));
    expcount->setString(std::to_string(xp) + "/" + std::to_string(maxXp));
    addChild(expcount);

    //listener
    EventListenerTouchOneByOne *listener2 = EventListenerTouchOneByOne::create();
    listener2->onTouchBegan = CC_CALLBACK_2(GamePlay::onTouchBegan2, this);
    listener2->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener2, this);

    //physics contact
    auto contactListener = EventListenerPhysicsContact::create();
    contactListener->onContactBegin = CC_CALLBACK_1(GamePlay::onContactBegin, this);
    this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(contactListener, this);

    //water icon
    watertoggle = MenuItemImage::create("water-off-toggled.png", "water-off.png", CC_CALLBACK_1(GamePlay::waterOff, this));
    watertoggle->setPosition(Vec2(origin.x + visibleSize.width/8, visibleSize.height*0.07));
    watertoggle->setScale(0.5);
    toolbarMenu= Menu::create(watertoggle, NULL);
    toolbarMenu->setPosition(Point(0,0));
    addChild(toolbarMenu, 1);

    //sprinkler count
    sprinkCounter = cocos2d::Label::createWithTTF("", "fonts/Ubuntu-B.ttf", 8);
    sprinkCounter->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    sprinkCounter->setString("Sprinklers: " + std::to_string(sprinkcount));
    sprinkCounter->setPosition(Vec2(origin.x + visibleSize.width * 0.5, visibleSize.height * 0.07));
    addChild(sprinkCounter);

    //water bonus
    int waterBonusText = 1 + (effect1+effect2+effect3+effect4+effect5);
    waterBonus = cocos2d::Label::createWithTTF("", "fonts/Ubuntu-B.ttf", 8);
    waterBonus->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    waterBonus->setString("Water pwr: +" + std::to_string(waterBonusText));
    waterBonus->setPosition(Vec2(origin.x + visibleSize.width * 0.5, visibleSize.height * 0.03));
    addChild(waterBonus);

    //back to menu
    auto menuitemback = MenuItemImage::create("buttonback.png", "buttonback.png", CC_CALLBACK_1(GamePlay::BacktoMenu2, this));
    auto menuitemshop = MenuItemImage::create("buttonshop.png", "buttonshop.png", CC_CALLBACK_1(GamePlay::ToShop, this));
    menuitemback->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.95));
    menuitemshop->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.85));
    menuitemback->setScale(0.7);
    menuitemshop->setScale(0.7);
    auto *menuback = Menu::create(menuitemback, menuitemshop, NULL);
    menuback->setPosition(Point(0,0));
    this->addChild(menuback);


    return true;
} //end init


//water on/off
void GamePlay::waterOff(Ref* pSender)
{
    if (Switch == 0) {
        watertoggle->setNormalImage(Sprite::create("water.png"));
        water = 1;
        Switch = 1;
    }
    else {
        watertoggle->setNormalImage(Sprite::create("water-off-toggled.png"));
        water = 0;
        Switch = 0;
    }

}

//water on/off effect
bool GamePlay::onTouchBegan2(cocos2d::Touch *touch, cocos2d::Event *event) {

    auto visibleSize = Director::getInstance()->getVisibleSize();
    Point touchLoc = touch->getLocation();
    if (water == 1) {
        {
            waterDroplet = Sprite::create("pisara.png");
            waterDroplet->setAnchorPoint(Vec2(0, 0));
            waterDroplet->setPosition(Point(touchLoc.x, touchLoc.y));

            auto spriteBody = PhysicsBody::createBox(waterDroplet->getContentSize(),
                                                          PhysicsMaterial(0, 1, 0));
            spriteBody->setVelocity(Vect(0, -220));
            //spriteBody->setCollisionBitmask(1);
            spriteBody->setContactTestBitmask(true);
            waterDroplet->setPhysicsBody(spriteBody);
            this->addChild(waterDroplet);
        }
        {
            auto spriteBody = PhysicsBody::createBox(tree->getContentSize(),
                                                     PhysicsMaterial(0, 1, 0));
            //spriteBody->setCollisionBitmask(2);
            spriteBody->setContactTestBitmask(true);
            spriteBody->setDynamic(false);
            tree->setPhysicsBody(spriteBody);

        }

        particles = ParticleGalaxy::create();
        particles->setPositionType(ParticleSystem::PositionType::RELATIVE);
        particles->setSourcePosition(Point((touchLoc.x - (visibleSize.width+50)), (touchLoc.y - (visibleSize.height/2))));
        particles->setDuration(0.25);
        addChild(particles, 20);

        auto action = Sequence::createWithTwoActions(
                DelayTime::create(0.5),
                RemoveSelf::create());

        waterDroplet->runAction(action);


        //collision
        /*Rect rectWater = waterDroplet->getBoundingBox();


        if (waterDroplet->getBoundingBox().intersectsRect(tree->getBoundingBox())) {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
        }*/
        return true;
    }
    return false;
}

void GamePlay::removeWater(float dt) {
    this->removeChild(waterDroplet, 1);
}

//back to menu
void GamePlay::BacktoMenu2(cocos2d::Ref *pSender) {
    def->setIntegerForKey("MONEY", money);
    def->setIntegerForKey("EXP", experience);
    def->setIntegerForKey("MAXEXP", maxExperience);
    def->setIntegerForKey("LEVEL", level);
    def->setFloatForKey("PERCENT", percent);
    def->flush();
    auto toMenu = HelloWorld::createScene();
    Director::getInstance()->replaceScene(toMenu);
}
//to shop
void GamePlay::ToShop(cocos2d::Ref *pSender) {
    def->setIntegerForKey("MONEY", money);
    def->setIntegerForKey("EXP", experience);
    def->setIntegerForKey("MAXEXP", maxExperience);
    def->setIntegerForKey("LEVEL", level);
    def->setFloatForKey("PERCENT", percent);
    def->flush();
    auto toshop = Shop::createScene();
    Director::getInstance()->replaceScene(toshop);
}
//check tree and drop collision
bool GamePlay::onContactBegin(cocos2d::PhysicsContact &contact) {
    effect = 1 + 1*(effect1+effect2+effect3+effect4+effect5); //change if want
    updateExp(effect);
    auto nodeA = contact.getShapeA()->getBody()->getNode();
    auto nodeB = contact.getShapeB()->getBody()->getNode();

    if (nodeA && nodeB)
    {
        if (nodeA->getTag() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
            updateMoney(level);
            moneycount->setString("$: " + std::to_string(money));
            nodeB->removeFromParentAndCleanup(true);
        }
        else if (nodeB->getTag() == 10)
        {
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
            updateMoney(level);
            moneycount->setString("$: " + std::to_string(money));
            nodeA->removeFromParentAndCleanup(true);
        }
    }

    //bodies can collide
    return true;

}

//water bill
void GamePlay::updateWater(float dt) {
    if (water == 1) {
        money = money - level;
        moneycount->setString("$: " + std::to_string(money));
    }
}
//update money over time
void GamePlay::sprinklerEffect(float dt) {
    if (sprinkcount >= 1)
        updateMoney(sprinkcount);
}
//update exp bar and count
void GamePlay::updateExp(int multiplier){
    experience = experience + multiplier;
    percent = ((experience / maxExperience) * 100);
    loadingbar->setPercent(percent);
    if (loadingbar->getPercent() >= 100) {
        percent = 0;
        loadingbar->setPercent(percent);
    }
    if (experience >= maxExperience) {
        if (treetype == 1)
            maxExperience = experience + round((4 * pow(level,3)) / 5 + 10);
        if (treetype == 2)
            maxExperience = experience + round((5 * pow(level,3)) / 4 + 10);
        if (treetype == 3)
            maxExperience = experience + round(pow(level,3) + 10);
        level++;
        tree->setTexture("tree" + std::to_string(level) + ".png");
        levelcount->setString("LVL: " + std::to_string(level));
        experience = 0;
        if (effect0) {
            hat->setPosition(Point(tree->getContentSize().width-15,tree->getContentSize().height-4));
        }
    }
    xp = experience;
    maxXp = maxExperience;
    expcount->setString(std::to_string(xp) + "/" + std::to_string(maxXp));
}
//update money
void GamePlay::updateMoney (int multiplier){
    money = money + multiplier;
    moneycount->setString("$: " + std::to_string(money));
}