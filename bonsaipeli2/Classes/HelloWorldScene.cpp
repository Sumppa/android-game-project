/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "SettingsScene.h"
#include "GameStartScene.h"
#include "GamePlayScene.h"
#include "ShopScene.h"
#include "HowToPlay.h"

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    return HelloWorld::create();
}
// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);
    //bg music
    def->getBoolForKey("MUSICVOLUME", true);
    def->getBoolForKey("SFXVOLUME", true);
    if (def->getBoolForKey("MUSICVOLUME", true) == true)
        CocosDenshion::SimpleAudioEngine::getInstance()->playBackgroundMusic("bg-musictrue.mp3", true);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //title
    labelmain = Label::createWithTTF("Bonsaigame", "fonts/Ubuntu-B.ttf", 14);
    if (labelmain == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        labelmain->setPosition(Vec2(origin.x + visibleSize.width/2,
                                    origin.y + visibleSize.height - labelmain->getContentSize().height));

        this->addChild(labelmain, 1);
    }

    //bg
    bg = Sprite::create("bg.png");
    bg->setAnchorPoint(Vec2(0,0));
    bg->setPosition(0,0);
    this->addChild(bg);


    //menu
    menuitem1 = MenuItemImage::create("buttonstart.png", "buttonstart.png", CC_CALLBACK_1(HelloWorld::Start, this));
    menuitem2 = MenuItemImage::create("buttonsettings.png", "buttonsettings.png", CC_CALLBACK_1(HelloWorld::Settings, this));
    menuitem3 = MenuItemImage::create("buttonhowtoplay.png", "buttonhowtoplay.png", CC_CALLBACK_1(HelloWorld::howToPlay, this));
    menuitem1->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height * 0.7));
    menuitem2->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height * 0.5));
    menuitem3->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height * 0.3));
    menuitem1->setScale(0.5);
    menuitem2->setScale(0.5);
    menuitem3->setScale(0.5);
    menumain = Menu::create(menuitem1, menuitem2, menuitem3, NULL);
    menumain->setPosition(Point(0, 0));
    this->addChild(menumain);

    return true;
} //end init


//start game
void HelloWorld::Start(cocos2d::Ref *pSender) {
    if (def->getBoolForKey("GAMESTATE", false)) {
        auto togame = GamePlay::createScene();
        Director::getInstance()->replaceScene(TransitionFade::create(1, togame));
    }
    else {
        auto togame = GameStart::createScene();
        Director::getInstance()->replaceScene(TransitionFade::create(1, togame));
    }
}

//go to settings
void HelloWorld::Settings(cocos2d::Ref *pSender) {
    auto tosetting = SettingsScene::createScene();
    Director::getInstance()->pushScene(tosetting);
}

//How To Play
void HelloWorld::howToPlay(cocos2d::Ref *pSender) {
    auto toHowToPlay = HowToPlay::createScene();
    Director::getInstance()->pushScene(toHowToPlay);
}
