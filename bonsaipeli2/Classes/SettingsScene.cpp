/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "SettingsScene.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"

USING_NS_CC;

Scene* SettingsScene::createScene()
{
    return SettingsScene::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool SettingsScene::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //music and sound variables
    SwitchMusic = def->getBoolForKey("MUSICVOLUME", true);
    SwitchSound = def->getBoolForKey("SFXVOLUME", true);

    //bg
    bg = Sprite::create("bg3.png");
    bg->setAnchorPoint(Vec2(0,0));
    bg->setPosition(0,0);
    this->addChild(bg);

    //back to menu
    back = MenuItemImage::create("buttonback.png", "buttonback.png", CC_CALLBACK_1(SettingsScene::BackHome, this));
    back->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.95));
    back->setScale(0.7);
    menuback = Menu::create(back, NULL);
    menuback->setPosition(Point(0,0));
    this->addChild(menuback);

    //set default settings for music and sound
    MenuItemFont::setFontSize(18);
    MenuItemFont::setFontName("fonts/Ubuntu-B.ttf");
    if (def->getBoolForKey("SFXVOLUME", true)) {
        menusound = MenuItemFont::create("Sound: On", CC_CALLBACK_1(SettingsScene::Sound, this));
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1);
    }
    else {
        menusound = MenuItemFont::create("Sound: Off", CC_CALLBACK_1(SettingsScene::Sound, this));
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0);
    }
    if (def->getBoolForKey("MUSICVOLUME", true))
        menumusic = MenuItemFont::create("Music: On", CC_CALLBACK_1(SettingsScene::Music, this));
    else
        menumusic = MenuItemFont::create("Music: Off", CC_CALLBACK_1(SettingsScene::Music, this));
    menudelete = MenuItemFont::create("Delete save", CC_CALLBACK_1(SettingsScene::Delete, this));
    menusound->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height/1.5));
    menumusic->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height/2));
    menudelete->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height/6));
    menusounditems = Menu::create(menusound, menumusic, menudelete, NULL);
    menusounditems->setPosition(Point(0,0));
    this->addChild(menusounditems);

    //title
    auto label = Label::createWithTTF("Settings", "fonts/Ubuntu-B.ttf", 16);
    if (label == nullptr)
    {
        problemLoading("'fonts/Ubuntu-B.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        label->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - label->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(label, 1);
    }

    // add the label as a child to this layer
    this->addChild(label, 1);
    return true;
} //end init

//back home
void SettingsScene::BackHome(cocos2d::Ref *pSender) {
    def->setBoolForKey("MUSICVOLUME", SwitchMusic);
    def->setBoolForKey("SFXVOLUME", SwitchSound);
    Director::getInstance()->popScene();
}

//sound switch
void SettingsScene::Sound(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
    if (SwitchSound == false) {
        menusound->setString("Sound: On");
        SwitchSound = true;
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(1);
    }
    else {
        menusound->setString("Sound: Off");
        SwitchSound = false;
        CocosDenshion::SimpleAudioEngine::getInstance()->setEffectsVolume(0);
    }
}
//music switch
void SettingsScene::Music(cocos2d::Ref *pSender) {
    CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
    if (SwitchMusic == false) {
        menumusic->setString("Music: On");
        SwitchMusic = true;
        CocosDenshion::SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    }
    else {
        menumusic->setString("Music: Off");
        SwitchMusic = false;
        CocosDenshion::SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    }
}
//delete save
void SettingsScene::Delete(Ref *pSender) {
    //shop values
    for (int i = 0; i <= 6; i++)
        def->deleteValueForKey(i + "BOUGHT");

    //item bought states
    def->deleteValueForKey("MTWATER_STATE");
    def->deleteValueForKey("HOLYWATER_STATE");
    def->deleteValueForKey("UNHOLYWATER_STATE");
    def->deleteValueForKey("WINE_STATE");
    def->deleteValueForKey("GODWINE_STATE");
    def->deleteValueForKey("HAT_STATE");
    //item effect states
    def->deleteValueForKey("MTWATER_FX");
    def->deleteValueForKey("HOLYWATER_FX");
    def->deleteValueForKey("UNHOLYWATER_FX");
    def->deleteValueForKey("WINE_FX");
    def->deleteValueForKey("GODWINE_FX");

    //sprink states
    def->deleteValueForKey("SPRINK_AM");
    def->deleteValueForKey("SPRINK_PRICE");

    //game values and options
    def->deleteValueForKey("GAMESTATE");
    def->deleteValueForKey("MONEY");
    def->deleteValueForKey("EXP");
    def->deleteValueForKey("MAXEXP");
    def->deleteValueForKey("LEVEL");
    def->deleteValueForKey("MUSICVOLUME");
    def->deleteValueForKey("SFXVOLUME");
    def->deleteValueForKey("PERCENT");
    def->flush();

    auto restart = HelloWorld::createScene();
    Director::getInstance()->replaceScene(restart);
}

