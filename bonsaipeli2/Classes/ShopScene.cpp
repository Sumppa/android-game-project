/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/


#include "HelloWorldScene.h"
#include "ShopScene.h"
#include "SimpleAudioEngine.h"
#include "Globals.h"
#include "SettingsScene.h"
#include "GameStartScene.h"
#include "GamePlayScene.h"
#include "Item.h"


USING_NS_CC;

Scene* Shop::createScene()
{
    return Shop::create();
}
// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in ShopScene.cpp\n");
}

// on "init" you need to initialize your instance
bool Shop::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //bg
    if (treetype == 1)
        bg = Sprite::create("bg_moon.png");
    else if (treetype == 2)
        bg = Sprite::create("bg_sun.png");
    else if (treetype == 3)
        bg = Sprite::create("bg2.png");
    bg->setPosition(Vec2(origin.x + visibleSize.width/2,
                         origin.y + visibleSize.height/2));
    this->addChild(bg);

    //current money
    currentmoney = cocos2d::Label::createWithTTF(std::to_string(money) + "$", "fonts/Ubuntu-B.ttf", 14);
    currentmoney->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    currentmoney->setPosition(Vec2(origin.x + visibleSize.width / 2,
                               origin.y + visibleSize.height / 13));
    addChild(currentmoney);

    //title
    labelmain = Label::createWithTTF("Shop", "fonts/Ubuntu-B.ttf", 14);
    labelmain->enableShadow(Color4B::BLACK, Size(1, -1), 1);
    if (labelmain == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        labelmain->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - labelmain->getContentSize().height));

        this->addChild(labelmain, 1);
    }

    //create items here
    item0_hat.description = "Santa Hat";
    item0_hat.price = 150;
    item0_hat.bought = def->getBoolForKey("HAT_STATE", false);
    item0_hat.info = "Xmas mood +999";

    item1_mtwater.description = "Mountain Water";
    item1_mtwater.price = 1000;
    item1_mtwater.effect = def->getIntegerForKey("MTWATER_FX", 2);
    item1_mtwater.bought = def->getBoolForKey("MTWATER_STATE", false);
    item1_mtwater.info = "Water +2";

    item2_holywater.description = "Holy Water";
    item2_holywater.price = 2000;
    item2_holywater.effect = def->getIntegerForKey("HOLYWATER_FX", 4);
    item2_holywater.bought = def->getBoolForKey("HOLYWATER_STATE", false);
    item2_holywater.info = "Water +4";

    item3_unholywater.description = "Unholy Water";
    item3_unholywater.price = 5000;
    item3_unholywater.effect = def->getIntegerForKey("UNHOLYWATER_FX", 8);
    item3_unholywater.bought = def->getBoolForKey("UNHOLYWATER_STATE", false);
    item3_unholywater.info = "Water +8";

    item4_wine.description = "Sacred Wine";
    item4_wine.price = 10000;
    item4_wine.effect = def->getIntegerForKey("WINE_FX", 20);
    item4_wine.bought = def->getBoolForKey("WINE_STATE", false);
    item4_wine.info = "Water +20";

    item5_godwine.description = "Wine of the Seven Gods";
    item5_godwine.price = 50000;
    item5_godwine.effect = def->getIntegerForKey("GODWINE_FX", 50);
    item5_godwine.bought = def->getBoolForKey("GODWINE_STATE", false);
    item5_godwine.info = "Water +50";

    item6_sprink.description = "Sprinkler";
    item6_sprink.effect = def->getIntegerForKey("SPRINK_AM", 0);
    item6_sprink.price = def->getIntegerForKey("SPRINK_PRICE", 100);
    item6_sprink.info = "Automatic money";

    //arrays for price, desc and info
    int itemprices[7] = {item0_hat.price,item1_mtwater.price,item2_holywater.price,item3_unholywater.price,item4_wine.price,item5_godwine.price,item6_sprink.price};
    std::string itemdescs[7] = {item0_hat.description, item1_mtwater.description,item2_holywater.description,item3_unholywater.description,item4_wine.description,item5_godwine.description,item6_sprink.description};
    std::string iteminfos[7] = {item0_hat.info, item1_mtwater.info,item2_holywater.info,item3_unholywater.info,item4_wine.info,item5_godwine.info,item6_sprink.info};

    //listview for shop items
    shopItemList = ui::ScrollView::create();
    shopItemList->setBackGroundImage("shopbox.png");
    shopItemList->setDirection(ui::ScrollView::Direction::VERTICAL);
    shopItemList->setContentSize(Size(160, 246));
    shopItemList->setInnerContainerSize(Size(1280, 350));
    shopItemList->setAnchorPoint(Vec2(0.5f,0.5f));
    shopItemList->setBounceEnabled('true');
    shopItemList->setPosition(Vec2(origin.x + visibleSize.width/2,
                                   origin.y + visibleSize.height/2));

    //loop shop items
    for (int i = 0; i <= 6; i++) {
        item1 = ui::Button::create("item" + std::to_string(i) + ".png", "itemtapped.png");
        itemprice[i] = Label::createWithTTF(def->getStringForKey(i + "BOUGHT", std::to_string(itemprices[i])), "fonts/Ubuntu-B.ttf", 7);
        itemdesc = Label::createWithTTF(itemdescs[i], "fonts/Ubuntu-B.ttf", 7);
        itemdesc->enableShadow(Color4B::BLACK, Size(1, -0.5), 1);
        iteminfo = Label::createWithTTF(iteminfos[i], "fonts/Ubuntu-B.ttf", 5);
        item1->addChild(itemprice[i]);
        item1->addChild(itemdesc);
        item1->addChild(iteminfo);
        item1->addClickEventListener([=](Ref* _sender)
                                      {
                                          ItemBought(i);
                                      });
        item1->setPosition(Vec2(shopItemList->getContentSize().width/4, i * 50));
        item1->setAnchorPoint(Vec2(0.5f,0));
        itemprice[i]->setPosition(Vec2(shopItemList->getContentSize().width/1.8, 10));
        itemdesc->setPosition(Vec2(shopItemList->getContentSize().width/1.8, 30));
        iteminfo->setPosition(Vec2(shopItemList->getContentSize().width/1.8, 20));
        shopItemList->addChild(item1);

    }

    this->addChild(shopItemList);

    //back to game
    menuitembacktogame = MenuItemImage::create("buttonback.png", "buttonback.png", CC_CALLBACK_1(Shop::BackToGame, this));
    menuitembacktogame->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.1));
    menuitembacktogame->setScale(0.7);
    menubacktogame = Menu::create(menuitembacktogame, NULL);
    menubacktogame->setPosition(Point(0,0));
    this->addChild(menubacktogame);
    return true;
} //end init

//check if item is bought
void Shop::ItemBought(int itemid) {
    switch(itemid) {
        case 0:
            if (!item0_hat.bought && money >= item0_hat.price) {
                //set flags
                item0_hat.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("HAT_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect0 = item0_hat.bought;
                money = money - item0_hat.price;
                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 1:
            if (!item1_mtwater.bought && money >= item1_mtwater.price) {
                //set flags
                item1_mtwater.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("MTWATER_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect1 = item1_mtwater.effect;
                money = money - item1_mtwater.price;
                def->setIntegerForKey("MTWATER_FX", effect1);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 2:
            if (!item2_holywater.bought && money >= item2_holywater.price) {
                //set flags
                item2_holywater.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("HOLYWATER_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect2 = item2_holywater.effect;
                money = money - item2_holywater.price;
                def->setIntegerForKey("HOLYWATER_FX", effect2);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 3:
            if (!item3_unholywater.bought && money >= item3_unholywater.price) {
                //set flags
                item3_unholywater.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("UNHOLYWATER_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect3 = item3_unholywater.effect;
                money = money - item3_unholywater.price;
                def->setIntegerForKey("UNHOLYWATER_FX", effect3);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 4:
            if (!item4_wine.bought && money >= item4_wine.price) {
                //set flags
                item4_wine.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("WINE_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect4 = item4_wine.effect;
                money = money - item4_wine.price;
                def->setIntegerForKey("WINE_FX", effect4);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 5:
            if (!item5_godwine.bought && money >= item5_godwine.price) {
                //set flags
                item5_godwine.bought = true;
                itemprice[itemid]->setString("Sold out!");
                def->setBoolForKey("GODWINE_STATE", true);
                def->setStringForKey(itemid + "BOUGHT", "Sold out!");

                effect5 = item5_godwine.effect;
                money = money - item5_godwine.price;
                def->setIntegerForKey("GODWINE_FX", effect5);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
        case 6:
            if (money >= item6_sprink.price) {

                money = money - item6_sprink.price;
                item6_sprink.price = item6_sprink.price*1.5;
                itemprice[itemid]->setString(std::to_string(item6_sprink.price));
                sprinkcount = sprinkcount + 1;

                def->setIntegerForKey("SPRINK_AM", sprinkcount);
                def->setIntegerForKey("SPRINK_PRICE", item6_sprink.price);

                currentmoney->setString(std::to_string(money) + "$");
            }
            break;
    }
}

//start game
void Shop::Start(cocos2d::Ref *pSender) {
    auto returntogame = GameStart::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(1, returntogame));
}
//back to game
void Shop::BackToGame(cocos2d::Ref *pSender) {
    def->setIntegerForKey("MONEY", money);
    def->flush();
    auto returntogame = GamePlay::createScene();
    Director::getInstance()->replaceScene(returntogame);
}