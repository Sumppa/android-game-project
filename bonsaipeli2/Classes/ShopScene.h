/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __SHOP_SCENE_H__
#define __SHOP_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"
#include "ui/CocosGUI.h"
#include <CCScrollView.h>

class Shop : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    cocos2d::UserDefault *def = cocos2d::UserDefault::sharedUserDefault();
    // implement the "static create()" method manually
    CREATE_FUNC(Shop);

    //LATEN TESITÄ
    void ItemBought(int itemid);
    cocos2d::Menu *shopMenu;

    void Start(Ref *pSender);
    void BackToGame(Ref *pSender);
    void Item1Bought(Ref *pSender, cocos2d::ui::Widget::TouchEventType);
    bool item1bought = 1;
    void Item2Bought(Ref *pSender, cocos2d::ui::Widget::TouchEventType);
    void Item3Bought(Ref *pSender, cocos2d::ui::Widget::TouchEventType);
    void Item4Bought(Ref *pSender, cocos2d::ui::Widget::TouchEventType);
    void Item5Bought(Ref *pSender, cocos2d::ui::Widget::TouchEventType);

    cocos2d::Label *labelmain;
    cocos2d::Label *currentmoney;
    cocos2d::Label *itemprice[7];
    cocos2d::Label *itemdesc;
    cocos2d::Label *iteminfo;


    cocos2d::ui::ScrollView *shopItemList;
    cocos2d::MenuItemImage *menuitembacktogame;
    cocos2d::ui::Button *item1;
    cocos2d::MenuItemImage *item2;
    cocos2d::MenuItemImage *item3;
    cocos2d::MenuItemImage *item4;
    cocos2d::MenuItemImage *item5;
    cocos2d::Menu *menubacktogame;
    cocos2d::Sprite *bg;
    cocos2d::Sprite *nakki;


    bool onTouchBegan3(cocos2d::Touch *touch, cocos2d::Event *event);
    //create item here
};


#endif // __SHOP_SCENE_H__
