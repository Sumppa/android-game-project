//
// Created by K8966 on 26.11.2018.
//

#ifndef PROJ_ANDROID_ITEM_H
#define PROJ_ANDROID_ITEM_H
#include <string>
#include <iostream>

using namespace std;

class Item {
    public:
    string description;
    string info;
    int price;
    int effect;
    bool bought;
};
Item item0_hat;
Item item1_mtwater;
Item item2_holywater;
Item item3_unholywater;
Item item4_wine;
Item item5_godwine;
Item item6_sprink;


#endif //PROJ_ANDROID_ITEM_H
