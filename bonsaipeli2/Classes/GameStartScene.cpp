/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.

 http://www.cocos2d-x.org

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "GameStartScene.h"
#include "GamePlayScene.h"
#include "SimpleAudioEngine.h"
#include "HelloWorldScene.h"
#include "Globals.h"

USING_NS_CC;

Scene* GameStart::createScene()
{
    return GameStart::create();
}

// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool GameStart::init() {
    //////////////////////////////
    // 1. super init first
    if (!Scene::init()) {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);

    //background using sprites
    bg = Sprite::create("bg.png");
    bg->setAnchorPoint(Vec2(0,0));
    bg->setPosition(0,0);
    this->addChild(bg);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //sprites
    pouch = Sprite::create("pouch1.png");
    pouch2 = Sprite::create("pouch2.png");
    pouch3 = Sprite::create("pouch3.png");
    pouch->setPosition(Vec2(origin.x + visibleSize.width/2,340));
    pouch2->setPosition(Vec2(origin.x + visibleSize.width/2,340));
    pouch3->setPosition(Vec2(origin.x + visibleSize.width/2,340));
    pouch->setScale(0.5);
    pouch2->setScale(0.5);
    pouch3->setScale(0.5);
    this->addChild(pouch);
    this->addChild(pouch2);
    this->addChild(pouch3);

    //move sprites
    auto move = MoveBy::create(1.3, Point(-50, -220));
    auto move2 = MoveBy::create(1.3, Point(0, -220));
    auto move3 = MoveBy::create(1.3, Point(50, -220));
    pouch->runAction(move);
    pouch2->runAction(move2);
    pouch3->runAction(move3);

    //listener
    listener = EventListenerTouchOneByOne::create();
    listener->onTouchBegan = CC_CALLBACK_2(GameStart::onTouchBegan, this);
    listener->setSwallowTouches(true);
    _eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);

    //back to menu
    menuitemback = MenuItemImage::create("buttonback.png", "buttonback.png", CC_CALLBACK_1(GameStart::BacktoMenu, this));
    menuitemback->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.1));
    menuitemback->setScale(0.7);
    menuback = Menu::create(menuitemback, NULL);
    menuback->setPosition(Point(0,0));
    this->addChild(menuback);


    //label pick up
    labelpick = cocos2d::Label::createWithTTF("Please pick a bag, \n young one...", "fonts/Ubuntu-B.ttf", 14);

    if (labelpick == nullptr)
    {
        problemLoading("'fonts/Ubuntu-B.ttf'");
    }
    else
    {
        // position the label on the center of the screen
        labelpick->setPosition(Vec2(origin.x + visibleSize.width/2,
                                origin.y + visibleSize.height - labelpick->getContentSize().height));

        // add the label as a child to this layer
        this->addChild(labelpick, 1);
    }


    return true;
}//end init

//touch functionality
bool GameStart::onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event) {
    Vec2 origin = Director::getInstance()->getVisibleOrigin();
    auto visibleSize = Director::getInstance()->getVisibleSize();
    auto menuitemback2 = MenuItemFont::create("NO!", CC_CALLBACK_1(GameStart::Decline, this));
    auto menuitemconfirm = MenuItemFont::create("YES!", CC_CALLBACK_1(GameStart::Confirm, this));
    menuitemback2->setScale(0.5);
    menuitemconfirm->setScale(0.5);

    menuitemback2->setPosition(Vec2(origin.x + visibleSize.width/1.5,
                                    origin.y + visibleSize.height/8));
    menuitemconfirm->setPosition(Vec2(origin.x + visibleSize.width/2.7,
                                      origin.y + visibleSize.height/8));

    auto *menumain = Menu::create(menuitemback2, menuitemconfirm, NULL);
    menumain->setPosition(Point(0,0));

    auto move = MoveBy::create(0.5, Point(0, 50));
    Point touchLoc = touch->getLocation();
    auto labelconfirm = Label::createWithTTF("Pick this bag?", "fonts/Ubuntu-B.ttf", 14);
    labelconfirm->setPosition(Vec2(origin.x + visibleSize.width/2,
                                   origin.y + visibleSize.height/6));
    if(num == 0) {
        if (pouch->getBoundingBox().containsPoint(touchLoc)) {
            this->addChild(menumain);
            this->addChild(labelconfirm, 1);
            pouch->runAction(move);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
            def->setIntegerForKey("TREETYPE", 1);
            num = 1;
        }
        if (pouch2->getBoundingBox().containsPoint(touchLoc)) {
            this->addChild(menumain);
            this->addChild(labelconfirm, 1);
            pouch2->runAction(move);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
            def->setIntegerForKey("TREETYPE", 2);
            num = 1;
        }
        if (pouch3->getBoundingBox().containsPoint(touchLoc)) {
            this->addChild(menumain);
            this->addChild(labelconfirm, 1);
            pouch3->runAction(move);
            CocosDenshion::SimpleAudioEngine::getInstance()->playEffect("blop.mp3");
            def->setIntegerForKey("TREETYPE", 3);
            num = 1;
        }
    }


    return false;
}

//decline pouch
void GameStart::Decline(cocos2d::Ref *pSender) {
    _eventDispatcher->removeEventListener(listener);
    num = 0;
    anime = 1;
    auto restart = GameStart::createScene();
    Director::getInstance()->replaceScene(restart);
}

//confirm pouch
void GameStart::Confirm(cocos2d::Ref *pSender) {
    _eventDispatcher->removeEventListener(listener);
    auto play = GamePlay::createScene();
    Director::getInstance()->replaceScene(TransitionFade::create(0.5, play));
    //Director::getInstance()->popScene();
}

//back to menu
void GameStart::BacktoMenu(cocos2d::Ref *pSender) {
    _eventDispatcher->removeEventListener(listener);
    auto toMenu = HelloWorld::createScene();
    Director::getInstance()->pushScene(toMenu);
}