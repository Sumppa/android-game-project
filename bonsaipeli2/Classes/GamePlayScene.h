/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#ifndef __GAMEPLAY_SCENE_H__
#define __GAMEPLAY_SCENE_H__
#include "cocos2d.h"
#include "ui/CocosGUI.h"


class GamePlay : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();

    // a selector callback
    void menuCloseCallback(cocos2d::Ref* pSender);
    cocos2d::UserDefault *def = cocos2d::UserDefault::sharedUserDefault();

    // implement the "static create()" method manually
    CREATE_FUNC(GamePlay);

    void sprinklerEffect(float dt);
    void updateExp(int multiplier);
    void updateMoney (int multiplier);

    void Start(Ref *pSender);
    void Settings(Ref *pSender);
    void BacktoMenu2(Ref *pSender);
    void ToShop(Ref *pSender);


    bool onTouchBegan2(cocos2d::Touch *touch, cocos2d::Event *event);
    void removeWater(float dt);
    void updateWater(float dt);

    int number;
    int water = 0;
    int Switch = 0;
    float percent;
    float barWidth;
    float scaleFactor;

    cocos2d::ui::LoadingBar *loadingbar;
    cocos2d::Label *moneycount;
    cocos2d::Label *levelcount;
    cocos2d::Label *expcount;
    cocos2d::Sprite *bg;
    cocos2d::Sprite *waterDroplet;
    cocos2d::Sprite *seed;
    cocos2d::Sprite *tree;
    cocos2d::Sprite *hat;
    cocos2d::MenuItemImage *watertoggle;
    cocos2d::Menu *toolbarMenu;
    cocos2d::ParticleSystem *particles;
    cocos2d::Label *sprinkCounter;
    cocos2d::Label *waterBonus;

    void waterOn(Ref *pSender);
    void waterOff(Ref *pSender);

    //TESTIÄ
    cocos2d::PhysicsWorld * sceneWorld;

    void SetPhysicsWorld(cocos2d::PhysicsWorld *world) {sceneWorld = world;}

    bool onContactBegin(cocos2d::PhysicsContact &contact);

};


#endif // __GAMEPLAY_SCENE_H__
