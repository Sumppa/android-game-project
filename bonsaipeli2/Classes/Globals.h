//
// Created by K8297 on 1.11.2018.
//

#ifndef PROJ_ANDROID_GLOBALS_H
#define PROJ_ANDROID_GLOBALS_H

#include <openssl/include/android/openssl/asn1.h>

extern int treetype;
extern long money;
extern int level;
extern float experience;
extern float maxExperience;
extern int itemprice;
extern long int xp;
extern long int maxXp;
extern int effect;
extern bool effect0;
extern int effect1;
extern int effect2;
extern int effect3;
extern int effect4;
extern int effect5;
extern int sprinkcount;

#endif //PROJ_ANDROID_GLOBALS_H
