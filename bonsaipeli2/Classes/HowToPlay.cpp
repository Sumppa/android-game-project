//
// Created by K8920 on 10.12.2018.
//

#include "HowToPlay.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;

Scene* HowToPlay::createScene()
{
    return HowToPlay::create();
}
// Print useful error message instead of segfaulting when files are not there.
static void problemLoading(const char* filename)
{
    printf("Error while loading: %s\n", filename);
    printf("Depending on how you compiled you might have to add 'Resources/' in front of filenames in HelloWorldScene.cpp\n");
}

// on "init" you need to initialize your instance
bool HowToPlay::init()
{
    //////////////////////////////
    // 1. super init first
    if ( !Scene::init() )
    {
        return false;
    }

    //disables debug info
    Director::getInstance()->setDisplayStats(false);

    //screen size
    auto visibleSize = Director::getInstance()->getVisibleSize();
    Vec2 origin = Director::getInstance()->getVisibleOrigin();

    //label
    howToLabel = Label::createWithTTF("How to play:", "fonts/Ubuntu-B.ttf", 14);
    if (howToLabel == nullptr)
    {
        problemLoading("'fonts/Marker Felt.ttf'");
    }
    else
    {
        howToLabel->setPosition(Vec2(origin.x + visibleSize.width/2,
                                    origin.y + visibleSize.height - howToLabel->getContentSize().height));

        this->addChild(howToLabel, 1);
    }

    //text
    howToText1 = Label::createWithTTF("Welcome.\n\nThe object of this game is to grow a tree.\nSee how far you can take it!\n\n\nYour tree will grow differently \nbased on the seed you choose.\n\nTo water your tree you must turn on \nthe water, BUT be aware of the water bill.\n\nUse your hard earned money \nto upgrade the quality of your water \nor even buy some automated sprinklers.", "fonts/Ubuntu-B.ttf", 8);
    howToText1->setPosition(Vec2(origin.x + visibleSize.width/2,
                                 origin.y + visibleSize.height * 0.6));
    this->addChild(howToText1, 1);

    //bg
    bg = Sprite::create("bg_moon.png");
    bg->setAnchorPoint(Vec2(0.5f,0.5f));
    bg->setPosition(Vec2(origin.x + visibleSize.width/2,
                         origin.y + visibleSize.height/2));
    this->addChild(bg);

    //back button
    backToMenu = MenuItemImage::create("buttonback.png", "buttonback.png", CC_CALLBACK_1(HowToPlay::BackHome, this));
    backToMenu->setPosition(Vec2(origin.x + visibleSize.width/1.1, visibleSize.height*0.95));
    backToMenu->setScale(0.7);
    menuBack = Menu::create(backToMenu, NULL);
    menuBack->setPosition(Point(0,0));
    this->addChild(menuBack);
    return true;
} //end init

//back home
void HowToPlay::BackHome(cocos2d::Ref *pSender) {
    Director::getInstance()->popScene();
}

