//
// Created by K8920 on 10.12.2018.
//

#ifndef __HOWTOPLAY_SCENE_H__
#define __HOWTOPLAY_SCENE_H__

#include "cocos2d.h"
#include "SimpleAudioEngine.h"

class HowToPlay : public cocos2d::Scene
{
public:
    static cocos2d::Scene* createScene();
    virtual bool init();

    // implement the "static create()" method manually
    CREATE_FUNC(HowToPlay);

    cocos2d::Sprite *bg;
    cocos2d::Label *howToLabel;
    cocos2d::Label *howToText1;
    cocos2d::MenuItemImage *backToMenu;
    cocos2d::Menu *menuBack;

    void BackHome(Ref *pSender);
};


#endif // __HOWTOPLAY_SCENE_H__
